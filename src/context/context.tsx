import React, { createContext, useContext, useEffect, useState } from 'react'

const GlobalContext = createContext<GlobalController>({} as GlobalController)

interface Props {
    children: any;
}

const GlobalContextProvider = (props: Props) => {
    const [data, setData] = useState<GlobalState>({
        
    })
   
    const globalState: GlobalController = {
        state: data
    }

    return (
        <GlobalContext.Provider value={globalState}>
            {props.children}
        </GlobalContext.Provider>
    )
}

export interface GlobalController {
    state: GlobalState
}

export interface GlobalState {
    
}


export default GlobalContextProvider

export const useGlobalContext = () => useContext<GlobalController>(GlobalContext)